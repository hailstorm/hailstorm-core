import express from 'express';

const app = express();

const port = 3000;

app.get('/', (req, res) => {
  res.send('Hailstorm');
});

app.listen(port, () => {
  console.log(`App is running at http://localhost:${port}`);
  console.log('Press Ctrl-C to stop');
});
