Hailstorm Core
==============

The core package for Hailstorm: a project dashboard *that works*.

For more information see https://gitlab.com/hailstorm/about
